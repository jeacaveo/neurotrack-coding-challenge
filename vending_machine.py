from constants import CENTS
from entities import Item, Slot, VendingMachine

if __name__ == "__main__":
    slots = {
        "A": Slot(item=Item(name="CandyBar", price=2.0), starting_capacity=2),
        "B": Slot(item=Item(name="Chips", price=1.5), starting_capacity=2),
        "C": Slot(item=Item("Soda", price=1.0), starting_capacity=2),
    }
    vm_obj = VendingMachine(slots=slots)
    print("Welcome to the Ultimate Vending Machine!")
    while vm_obj.get_available_items():
        print(f"Please make a selection:")
        print(
            "\n".join(
                [
                    f"{item.slot} -> {item.name}\t({item.price})"
                    for item in vm_obj.get_available_items()
                ]
            )
        )
        selection = input("Your selection: ").upper()
        payment = input("How are you paying?: ")
        change = vm_obj.pay_item(selection, payment)  # TODO handle exceptions
        print(
            f"Item delivered! {'Here is your change:' if change else 'No change needed, thanks!'}"
        )
        if change.items():
            for coin, amount in change.items():
                print(f"{amount}x {CENTS[coin].name}(s)")
    print("Vending machine is empty, please come back later!")
