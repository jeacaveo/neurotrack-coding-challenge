from collections import namedtuple
from exceptions import (
    ExcessCapacityError,
    InvalidDenominationError,
    InvalidDenominationAmountError,
)
from typing import Dict, List, Optional

from constants import CENTS

# Using a separate structure to combine Item and Slot for clarity and consistency
MenuItem = namedtuple("MenuItem", ["slot", "name", "price"])


class Item:
    name: str
    price: float

    def __init__(self, name: str, price: float) -> None:
        self.name = name
        self.price = price


class Slot:
    item: Item
    current_capacity = int
    max_capacity: int

    def __init__(
        self, item: Item, starting_capacity: int, max_capacity: int = 10
    ) -> None:
        if starting_capacity > max_capacity:
            raise ExcessCapacityError("Starting capacity exceeds max capacity")

        self.max_capacity = max_capacity
        self.item = item
        self.current_capacity = starting_capacity


class VendingMachine:
    slots: Dict[str, Optional[Slot]] = {"A": None, "B": None, "C": None, "D": None}
    bills_box: Dict[str, int] = {"500": 0, "1000": 0, "2000": 0, "5000": 0, "10000": 0}
    # TODO coins box should include coins based on items at 'startup'
    coins_box: Dict[str, int] = {"5": 200, "10": 100, "25": 50, "100": 50, "200": 50}

    def __init__(self, slots: Optional[Dict[str, Slot]]) -> None:
        # TODO add validation for provided slots to match the configuration for the
        # vending machine (don't allow for extra slots to be added unless expected).
        self.slots.update(slots)

    def get_available_items(self) -> List[MenuItem]:
        return [
            MenuItem(slot_name, slot.item.name, slot.item.price)
            for slot_name, slot in self.slots.items()
            if slot and slot.current_capacity > 0
        ]

    def pay_item(self, slot_name: str, payment: str) -> Dict[str, int]:
        # TODO validate proper payment coin/bill, slot name and availability
        # TODO consider multipel bills or coins as payment
        slot = self.slots[slot_name]
        slot.current_capacity -= 1

        # TODO add test for invalid denomination and show available options
        try:
            denomination = CENTS[payment]
        except KeyError:
            raise InvalidDenominationError(
                "Payment type is not supported, please use a valid Canadian denomination."
            )

        if denomination.type == "bill":
            self.bills_box[payment] += 1
        else:
            self.coins_box[payment] += 1

        # TODO change calculator would be a different function but no time for
        # testing so leaving it here.
        change = {}
        remaining = denomination.value - (slot.item.price * 100)
        if remaining < 0:
            raise InvalidDenominationAmountError(
                "Payment provided is less than the item's price"
            )

        for coin, availability in dict(
            sorted(self.coins_box.items(), key=lambda coin: int(coin[0]), reverse=True)
        ).items():
            coins_amount = int(remaining / int(coin))
            if coins_amount > 0:
                # TODO consider coin availability
                self.coins_box[coin] -= coins_amount
                remaining -= int(coin) * coins_amount
                change.update({coin: coins_amount})
            if remaining == 0:
                break

        return change
