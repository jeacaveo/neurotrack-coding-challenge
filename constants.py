from collections import namedtuple

Denomination = namedtuple("Denomination", ["value", "type", "name"])

CENTS = {
    "5": Denomination(5, "coin", "Nickel"),
    "10": Denomination(10, "coin", "Dime"),
    "25": Denomination(25, "coin", "Quarter"),
    "100": Denomination(100, "coin", "Loonie"),
    "200": Denomination(200, "coin", "Toonie"),
    "500": Denomination(500, "bill", "5 dollar bill"),
    "1000": Denomination(1000, "bill", "10 dollar bill"),
    "2000": Denomination(2000, "bill", "20 dollar bill"),
    "5000": Denomination(5000, "bill", "50 dollar bill"),
    "10000": Denomination(10000, "bill", "100 dollar bill"),
}
