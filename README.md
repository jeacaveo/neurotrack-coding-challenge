## Neurotrack Coding Challenge
 
Welcome to the Neurotrack coding challenge! The intent of this challenge is just to see how you think, and how you structure a solution to a problem. We don't want this to be burdensome; do not spend more than 2 hours on this!

Design a vending machine program. I would like to be able to view the vending machine menu, select an item, and pay/receive change.

- Assume you can give change in [5, 10, 25, 100, 200] cent coin denominations (Canadian currency of nickel, dime, quarter, 1 dollar coin, 2 dollar coin)
- Assume initial menu of {'CandyBar':200, ‘Chips’:150, ‘Soda’:100}
- Assume people can pay with standard Canadian currency
- Outline any shortcuts, TODOs, or things you would address if given more time
 
When complete, publish your solution to github/gitlab, and send us the link along with instructions on how to run the program. 

## Shortcuts/TODOs

Total time taken: ~2.5 hours

Because of time contraints I had to leave some things out:

- Made the assumption it was just a simple console program, depending on the scope other components would've been added/considered (databases or other storage tools, graphical interfaces, etc.).
- Added TODOs along the code for some of the things I skipped on because of time.
- Would have added more and better tests.
- Would have added better error handling and messages.
- Most of the entities could be expanded upon to add more attributes and/or functionality.
- Would have added additional checks (see TODOs for some examples).
- Would've made the 'user interface' for user friendly.

## How to run

- Go to root of project:

        cd neurotrack-coding-challenge/

- Install requirements, leaving the environment management up to you (Python 3.10):

        pip3 install -r requirements.txt

- To run tests:

        python3 -m pytest tests.py -v
    
- To run program:

        python3 vending_machine.py

- Only the options available for items should be used (error can pop up if not):

- For the available payment denominations, please look at `constants.py` file.
