class ExcessCapacityError(Exception):
    pass


class InvalidDenominationError(Exception):
    pass


class InvalidDenominationAmountError(Exception):
    pass
