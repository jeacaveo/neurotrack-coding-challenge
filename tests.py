import pytest

from entities import Item, MenuItem, Slot, VendingMachine
from exceptions import ExcessCapacityError

from constants import CENTS


def test_item_create():
    # Given
    name = "item1"
    price = 1.0

    # When
    obj = Item(name=name, price=price)

    # Then
    assert obj.name == name
    assert obj.price == price


def test_slot_create():
    # Given
    item = Item("item1", 1.0)
    start_amount = 8

    # When
    obj = Slot(item=item, starting_capacity=start_amount)

    # Then
    assert obj.item == item
    assert obj.current_capacity == start_amount
    assert obj.max_capacity == 10


def test_slot_bad_starting_capacity():
    # Given
    item = Item("item1", 1.0)
    start_amount = 11

    # When
    with pytest.raises(ExcessCapacityError) as exc:
        Slot(item=item, starting_capacity=start_amount)

    # Then
    assert str(exc.value) == "Starting capacity exceeds max capacity"


def test_vending_machine_create():
    # Given
    slots = {
        "B": Slot(item=Item(name="item1", price=1.5), starting_capacity=10),
        "D": Slot(item=Item("item2", price=5.0), starting_capacity=10),
    }

    # When
    obj = VendingMachine(slots=slots)

    # Assert
    assert obj.bills_box == {"500": 0, "1000": 0, "2000": 0, "5000": 0, "10000": 0}
    assert obj.coins_box == {"5": 200, "10": 100, "25": 50, "100": 50, "200": 50}
    assert obj.slots == {"A": None, "B": slots["B"], "C": None, "D": slots["D"]}


def test_vending_machine_menu():
    # Given
    slots = {
        "B": Slot(item=Item(name="item1", price=1.5), starting_capacity=10),
        "C": Slot(item=Item(name="item1", price=1.5), starting_capacity=0),
        "D": Slot(item=Item("item2", price=5.0), starting_capacity=10),
    }
    expected_item1 = MenuItem("B", "item1", 1.5)
    expected_item2 = MenuItem("D", "item2", 5.0)

    # When
    menu = VendingMachine(slots=slots).get_available_items()

    # Assert
    assert menu == [expected_item1, expected_item2]


def test_vending_machine_payment():
    # Given
    slots = {
        "B": Slot(item=Item(name="item1", price=1.5), starting_capacity=10),
        "C": Slot(item=Item(name="item1", price=1.5), starting_capacity=0),
        "D": Slot(item=Item("item2", price=5.0), starting_capacity=10),
    }
    selection = "D"
    payment = "2000"  # 20 dollar bill
    expected_change = {"200": 7, "100": 1}

    # When
    vm_obj = VendingMachine(slots=slots)
    assert vm_obj.coins_box["100"] == 50
    assert vm_obj.coins_box["200"] == 50
    assert vm_obj.bills_box[payment] == 0
    change = vm_obj.pay_item(selection, payment)

    # Assert
    assert vm_obj.slots[selection].current_capacity == 9
    assert vm_obj.bills_box[payment] == 1
    assert change == expected_change
    assert vm_obj.coins_box["100"] == 49
    assert vm_obj.coins_box["200"] == 43
